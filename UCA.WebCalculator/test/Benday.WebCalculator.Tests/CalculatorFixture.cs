﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using UCA.WebCalculator.Api;

namespace UCA.WebCalculator.Tests
{
    [TestClass]
    public class CalculatorFixture
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }
        
        private Calculator _SystemUnderTest;
        public Calculator SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }

                return _SystemUnderTest;
            }
        }

        [TestMethod]
        public void Add()
        {
            // arrange
            double value1 = 2;
            double value2 = 3;
            double expected = 5;

            // act
            double actual = SystemUnderTest.Add(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }


       


        [TestMethod]
        public void Subtract()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 4;

            // act
            double actual = SystemUnderTest.Subtract(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Multiply()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 12;

            // act
            double actual = SystemUnderTest.Multiply(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Divide()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 3;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivideByZeroThrowsException()
        {
            // arrange
            double value1 = 6;
            double value2 = 0;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);
        }
         
        private IWebDriver driver;
        public string homeURL;
        public List<string> Acciones = new List<string> { "Add", "Subtract", "Multiply", "Divide" };
        Random random = new Random();

        [TestMethod]
        public void TestSite_ADD() {
            driver = new ChromeDriver(@"C:/Driver/");
            homeURL = "https://localhost:44348/calculator";

            driver.Navigate().GoToUrl(homeURL);

            Acciones.ForEach(x => {
                driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
                driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys(random.Next(0, 100).ToString());
                var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
                var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

                selectElementa.SelectByValue(x);

                driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
                driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys(random.Next(0, 100).ToString());

                driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();
                //Assert.Equals(Message_Success,)
            });
            


            
            driver.Close();
        }


        [TestMethod]
        public void TestSite_Subtract()
        {
            driver = new ChromeDriver(@"C:/Driver/");
            homeURL = "https://localhost:44348/calculator";

            driver.Navigate().GoToUrl(homeURL);

            
                driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
                driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys(random.Next(0, 100).ToString());
                var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
                var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

                selectElementa.SelectByValue("Subtract");

                driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
                driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys(random.Next(0, 100).ToString());

                driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();
          



            driver.Close();
        }

        [TestMethod]
        public void TestSite_Multiply()
        {
            driver = new ChromeDriver(@"C:/Driver/");
            homeURL = "https://localhost:44348/calculator";

            driver.Navigate().GoToUrl(homeURL);


            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys(random.Next(0, 100).ToString());
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Multiply");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys(random.Next(0, 100).ToString());

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();




            driver.Close();
        }

        [TestMethod]
        public void TestSite_Divide()
        {
            driver = new ChromeDriver(@"C:/Driver/");
            homeURL = "https://localhost:44348/calculator";

            driver.Navigate().GoToUrl(homeURL);


            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys(random.Next(0, 100).ToString());
            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Divide");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys(random.Next(0, 100).ToString());

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();




            driver.Close();
        }

    }
}
